import machine
import utime
#https://wiki.dfrobot.com/URM09_Ultrasonic_Sensor_Gravity_Trig_SKU_SEN0388

ultrasonicPin=17 # D10 ESP32 

while True:
    trig = machine.Pin(ultrasonicPin, machine.Pin.OUT)
    trig.value(0)
    utime.sleep_us(10)
    trig.value(1)
    utime.sleep_us(10)
    trig.value(0)

    pulse = machine.Pin(ultrasonicPin, machine.Pin.IN)

    while pulse.value() == 0:
        pass
    start = utime.ticks_us()
    while pulse.value() == 1:
        pass
    end = utime.ticks_us()
    
    width = end-start

    speed_cmus = ( ( 331.5 + 0.6 * (float)( 20 ) ) * 100 / 1000000.0 )
    # celsius, speed of sound (cm/us)
    
    distance = speed_cmus * width / 2
    print(distance)
    
    utime.sleep_us(555000)
