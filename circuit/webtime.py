import initi
cfgData=initi.cfgDataLoad()
requests = initi.wifi(cfgData)
response = requests.get(cfgData['webtime.url'])
dateStr = response.headers['date']
#                       01234567
#Date: Sat, 30 Apr 2022 04:41:38 GMT
#      0     1 2    3   4 
numMonth={'Jan':1,
    'Feb':2,
    'Mar':3,
    'Apr':4,
    'May':5,
    'Jun':6,
    'Jul':7,
    'Aug':8,
    'Sep':9,
    'Oct':10,
    'Nov':11,
    'Dec':12,
}
dateParts = dateStr.split(' ')
import time
tm = time.struct_time((
    int(dateParts[3]), #year
    numMonth[dateParts[2]],
    int(dateParts[1]),
    int(dateParts[4][0:2]),#hour
    int(dateParts[4][3:5]),
    int(dateParts[4][6:8]),
    0,#day of week 0=mon
    -1,#day of year
    -1,#dst
))
import rtc
r=rtc.RTC()
r.datetime=tm
