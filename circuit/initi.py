def cfgDataSave(cfgData):
    fh=open('data.txt','w')
    for k,v in cfgData.items():
        fh.write('%s=%s\n'%(k,v))
    fh.close()

def cfgDataLoad():
    cfgData={}
    try:
        fh=open('data.txt', 'r')
        for l in fh:
            (k,nil,v)=l.strip().partition('=')
            cfgData[k.strip()]=v.strip()
        fh.close()
    except OSError:
        pass
    return cfgData

#boot.py
#import initi
#cfgData=initi.cfgDataLoad()
#cfgData['bootCount']=str(1+int(cfgData['bootCount']))
#import storage
#storage.remount('/',False)#readonly=False
#initi.cfgDataSave(cfgData)
#storage.remount('/',True)

wifi_requests = None
def wifi(cfgData=None):
    if wifi_requests != None:
        return wifi_requests
    import wifi
    import socketpool
    import adafruit_requests
    import ssl
    if cfgData == None:
        cfgData=cfgDataLoad()
    wifi.radio.connect(cfgData['wifi.ssid'], cfgData['wifi.password'])
    pool=socketpool.SocketPool(wifi.radio)
    wifi_requests = adafruit_requests.Session(pool, ssl.create_default_context())
    return wifi_requests
